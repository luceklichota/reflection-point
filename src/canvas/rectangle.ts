import {IPoint, IRectangle} from './model';
import {Point} from './point';
import {ICoordinates} from './receiver.model';

export class Rectangle {
  private angle;
  private dimensions: { width: number, length: number };
  private coordinates: ICoordinates;

  static getRectanglePoints(rectangle: IRectangle, rotationPoint: IPoint, angle: number): IPoint[] {
    return [
      Point.pointAfterRotation({
        x: rectangle.x,
        y: rectangle.y
      }, rotationPoint, angle),
      Point.pointAfterRotation({
        x: rectangle.x + rectangle.width,
        y: rectangle.y
      }, rotationPoint, angle),
      Point.pointAfterRotation({
        x: rectangle.x + rectangle.width,
        y: rectangle.y + rectangle.length
      }, rotationPoint, angle),
      Point.pointAfterRotation({
        x: rectangle.x,
        y: rectangle.y + rectangle.length
      }, rotationPoint, angle),
    ];
  }

  static getExtremeCoordinates(rectangle: IPoint[]) {
    let extremeCoords;
    rectangle.forEach(point => {
      if (point.x < extremeCoords.minX || !extremeCoords.minX) {
        extremeCoords.minX = point.x;
      }
      if (point.x > extremeCoords.maxX || !extremeCoords.maxX) {
        extremeCoords.maxX = point.x;
      }
      if (point.y > extremeCoords.maxY || !extremeCoords.maxY) {
        extremeCoords.maxY = point.y;
      }
      if (point.y < extremeCoords.minY || !extremeCoords.minY) {
        extremeCoords.minY = point.y;
      }
    });
    return extremeCoords;
  }




}
