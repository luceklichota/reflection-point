// import {IRoomDimensions} from './room.models';
// import {IOffset, ICoordinates} from './receiver.model';
// import {fromEvent} from 'rxjs';
// import {IPoint, IRectangle} from './model';
// import {filter, map} from 'rxjs/operators';
// import {Point} from './point';
// import {Rectangle} from './rectangle';
//
// export class Speakers {
//   constructor(private ctx: CanvasRenderingContext2D) {
//     fromEvent(document, 'mousedown').subscribe((event: MouseEvent) => {
//       const index = this.getIntersectionIndex(event);
//       if (index !== null) {
//         this.isDrag = true;
//         this.dragIndex = index;
//         const point = this.getPointRelativeToCanvas(event);
//         this.dragOffset = this.setDragOffset(point);
//       }
//     });
//
//     fromEvent(document, 'mouseup').subscribe(event => {
//       this.isDrag = false;
//     });
//   }
//   private scale = 1;
//   private roomDimensions: IRoomDimensions;
//   private receiverCoordinates: ICoordinates;
//   private speakerDimensions = {width: 21, length: 35, height: 31};
//   private roomOffset;
//   private coordinates;
//   private rectangles: IRectangle[];
//   private rectanglesPoints: IPoint[][];
//   private isDrag = false;
//   private dragIndex: number;
//   private dragOffset: IOffset;
//
//   static pointInsideRectangle(point, rectangle: IPoint[]) {
//     const x = point.x;
//     const y = point.y;
//
//     let inside = false;
//     for (let i = 0; i < rectangle.length; i++) {
//       const nextCorner = i === 3 ? 0 : i+ 1;
//       const xi = rectangle[i].x;
//       const yi = rectangle[i].y;
//       const xj = rectangle[nextCorner].x;
//       const yj = rectangle[nextCorner].y;
//
//       const intersect = ((yi > y) !== (yj > y))
//         && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
//
//       if (intersect) {
//         inside = !inside;
//       }
//     }
//
//     return inside;
//   }
//
//   public setCoordinates(coordinates: ICoordinates[]) {
//     this.coordinates = coordinates;
//   }
//
//   public setScale(scale: number) {
//     this.scale = scale;
//   }
//
//   public setRoomDimensions(roomDimensions: IRoomDimensions, roomOffset: number) {
//     this.roomDimensions = roomDimensions;
//     this.roomOffset = roomOffset;
//     this.setSquaresDimensions();
//   }
//
//   public setReceiverCoordinates(coordinates: ICoordinates) {
//     this.receiverCoordinates = coordinates;
//     this.setSquaresDimensions();
//     this.rectanglesPoints = this.getRectanglesPoints();
//   }
//
//   getIntersectionIndex(e: MouseEvent): number | null {
//     const point = {
//       x: e.clientX - this.ctx.canvas.offsetLeft,
//       y: e.clientY - this.ctx.canvas.offsetTop
//     };
//
//     let intersectionIndex = null;
//     this.rectanglesPoints.forEach((rectangle, index) => {
//       if (Speakers.pointInsideRectangle(point, rectangle)) {
//         intersectionIndex = index;
//       }
//     });
//
//     return intersectionIndex;
//   }
//
//   getOffsets() {
//     return fromEvent(document, 'mousemove').pipe(
//       filter(() => this.isDrag),
//       map((event: MouseEvent) => {
//         return this.getOffsetFromMousePosition(event);
//       })
//     );
//   }
//
//   getPointRelativeToCanvas(e: MouseEvent) {
//     return {
//       x: e.clientX - this.ctx.canvas.offsetLeft,
//       y: e.clientY - this.ctx.canvas.offsetTop
//     };
//   }
//
//   getOffsetFromMousePosition(e: MouseEvent) {
//     let coordinates: ICoordinates;
//     const point = this.getPointRelativeToCanvas(e);
//     const rectangle: IRectangle = {
//       x: point.x + this.dragOffset.x,
//       y: point.y + this.dragOffset.y,
//       width: this.speakerDimensions.width,
//       length: this.speakerDimensions.length,
//     };
//     const rectanglePoints = Rectangle.getRectanglePoints(rectangle, this.getTransmitterOffsets(rectangle), this.getSpeakerAngle(point));
//     console.log(rectanglePoints);
//
//     const pointWithinBoundaries = {
//       x: this.checkBoundariesX(rectanglePoints),
//       y: this.checkBoundariesY(rectanglePoints)
//     };
//     switch (this.dragIndex) {
//       case 2:
//         // coordinates = {
//         //   x: (pointWithinBoundaries.x - this.roomOffset) / this.scale,
//         //   y: (pointWithinBoundaries.y - this.roomOffset) / this.scale,
//         //   z: this.coordinates.z
//         // };
//         break;
//       case 1:
//         // coordinates = {
//         //   x: this.coordinates.x,
//         //   y: (this.roomDimensions.length - (pointWithinBoundaries.x - (this.roomOffset * 2) - this.roomDimensions.width)) / this.scale,
//         //   z: (this.roomDimensions.height - (pointWithinBoundaries.y - this.roomOffset)) / this.scale,
//         // };
//         // break;
//     }
//
//     return coordinates;
//   }
//
//   public getInitialCoordinates(roomDimensions: IRoomDimensions): ICoordinates[] {
//     return [
//       {
//         x: this.speakerDimensions.width + 50,
//         y: this.speakerDimensions.length + 100,
//         z: 120
//       },
//       {
//         x: roomDimensions.width - this.speakerDimensions.width - 50,
//         y: this.speakerDimensions.length + 100,
//         z: 120
//       }
//     ];
//
//   }
//
//   public getCoordinatesScaled(): ICoordinates[] {
//     return this.coordinates.map(coordinates => {
//       return Object.assign({}, coordinates,
//         {
//           x: coordinates.x * this.scale,
//           y: coordinates.y * this.scale,
//           z: coordinates.z * this.scale,
//         });
//     });
//   }
//
//   private getDimensionsScaled() {
//     return {
//       width: this.speakerDimensions.width * this.scale,
//       length: this.speakerDimensions.length * this.scale,
//       height: this.speakerDimensions.height * this.scale,
//     };
//   }
//
//   public setSquaresDimensions() {
//     const coordinatesScaled = this.getCoordinatesScaled();
//     const speakerDimensions = this.getDimensionsScaled();
//     this.rectangles = [];
//     coordinatesScaled.forEach((coordinate) => {
//       this.rectangles.push(
//         {
//           x: coordinate.x + this.roomOffset - (speakerDimensions.width / 2),
//           y: coordinate.y + this.roomOffset - speakerDimensions.length,
//           width: speakerDimensions.width,
//           length: speakerDimensions.length,
//         }
//       );
//       this.rectangles.push(
//         {
//           x: this.roomOffset + this.roomDimensions.width + this.roomOffset + (this.roomDimensions.length - coordinate.y),
//           y: (this.roomOffset + this.roomDimensions.height) - coordinate.z,
//           width: speakerDimensions.length,
//           length: speakerDimensions.height,
//         }
//       );
//     });
//   }
//
//   private getSpeakerAngle(speakerCoordinates) {
//     const x = speakerCoordinates.x - (this.receiverCoordinates.x + this.roomOffset);
//     const y = speakerCoordinates.y - (this.receiverCoordinates.y + this.roomOffset);
//
//     const angle = Math.atan(Math.abs(x / y));
//
//     if (x > 0 && y < 0) {
//       return angle;
//     } else if (x < 0 && y < 0) {
//       return -angle;
//     } else if (x < 0 && y > 0) {
//       return Math.PI + angle;
//     } else if (x > 0 && y > 0) {
//       return Math.PI - angle;
//     }
//   }
//
//   private getRectanglesPoints(): IPoint[][] {
//     const points = [];
//     this.rectangles.forEach(rectangle => {
//       const transmitterOffsets = this.getTransmitterOffsets(rectangle);
//       const angle = this.getSpeakerAngle(transmitterOffsets);
//       const rectanglePoints = [
//         Point.pointAfterRotation({
//           x: rectangle.x,
//           y: rectangle.y
//         }, transmitterOffsets, angle),
//         Point.pointAfterRotation({
//           x: rectangle.x + rectangle.width,
//           y: rectangle.y
//         }, transmitterOffsets, angle),
//         Point.pointAfterRotation({
//           x: rectangle.x + rectangle.width,
//           y: rectangle.y + rectangle.length
//         }, transmitterOffsets, angle),
//         Point.pointAfterRotation({
//           x: rectangle.x,
//           y: rectangle.y + rectangle.length
//         }, transmitterOffsets, angle),
//       ] as IPoint[];
//       points.push(rectanglePoints);
//     });
//
//
//     return points;
//   }
//
//   public getTransmitterOffsets(speakerOffsets) {
//     const speakerDimensions = this.getDimensionsScaled();
//     return {
//       x: speakerOffsets.x + (speakerDimensions.width / 2),
//       y: speakerOffsets.y + speakerDimensions.length
//     };
//   }
//
//
//   public draw() {
//     this.rectangles.forEach((rectangle, index) => {
//       if (index % 2 === 0) {
//         const transmitterOffsets = this.getTransmitterOffsets(rectangle);
//         const angle = this.getSpeakerAngle(transmitterOffsets);
//         this.ctx.translate(transmitterOffsets.x, transmitterOffsets.y);
//         this.ctx.rotate(angle);
//         this.ctx.translate(-transmitterOffsets.x, -transmitterOffsets.y);
//         this.ctx.beginPath();
//       }
//
//       this.ctx.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.length);
//       this.ctx.stroke();
//       this.ctx.setTransform(1, 0, 0, 1, 0, 0);
//     });
//   }
//
//
//   checkBoundariesY(y) {
//
//
//     return y;
//   }
//
//   checkBoundariesX(points) {
//
//   }
//
//   setDragOffset(pos) {
//     return {
//       x: this.rectangles[this.dragIndex].x - pos.x,
//       y: this.rectangles[this.dragIndex].y - pos.y,
//     };
//   }
//
//
// }
