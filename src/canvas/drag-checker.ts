import {ICircle, IPoint, IRectangle, IShape} from './model';
import {Circle} from './shapes/circle';
import {Rectangle} from './shapes/rectangle';


export class DragChecker {
  static isClicked(shapes: IShape[], click: IPoint) {
    let clickedShape: IShape | null = null;

    shapes.some((shape) => {
      switch (shape.type) {
        case 'circle':
          if (Circle.isClicked(<ICircle> shape, click)) {

            clickedShape = shape;
            return true;
          }
          break;
        case 'rectangle':
          if (Rectangle.isClicked(<IRectangle> shape, click)) {
            clickedShape = shape;
            return true;
          }
          break;
      }
    });

    return clickedShape;
  }
}
