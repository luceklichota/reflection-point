import {IBlock, IBox, ICircle, ILine, IPoint, IRectangle, IShape, ISphere} from './model';

export class BlockToShapeConverter {
  constructor(private scale: number, private zeroPoints: IPoint[]) {}

  convert(blocks: IBlock[]): IShape[] {
    const shapes = [];

    blocks.forEach((block: IBlock) => {
      switch (block.type) {
        case 'sphere':
          shapes.push(...this.calculateSphere(<ISphere> block));
          break;
        case 'box':
          shapes.push(...this.calculateBox(<IBox> block));
          break;
        case 'line':
          shapes.push(...this.calculateLine(<ILine> block));
          break;
      }
    });

    return shapes;
  }

  calculateSphere(sphere: ISphere) {
    return [
      {
        name: sphere.name,
        type: 'circle',
        plan: 'top',
        x: this.zeroPoints[0].x + sphere.x * this.scale,
        y: this.zeroPoints[0].y + sphere.y * this.scale,
        radius: sphere.radius * this.scale,
      },
      {
        name: sphere.name,
        type: 'circle',
        plan: 'left',
        x: this.zeroPoints[1].x + sphere.y * this.scale,
        y: this.zeroPoints[1].y + sphere.z * this.scale,
        radius: sphere.radius * this.scale,
      },
    ];
  }

  calculateBox(box: IBox) {
    return [
      {
        name: box.name,
        type: 'rectangle',
        plan: 'top',
        x: this.zeroPoints[0].x + box.x * this.scale,
        y: this.zeroPoints[0].y + box.y * this.scale,
        width: box.width * this.scale,
        length: box.length * this.scale,
        angle: box.angle,
        rotationPoint: box.rotationPoint,
      },
      {
        name: box.name,
        type: 'rectangle',
        plan: 'left',
        x: this.zeroPoints[1].x + box.y * this.scale,
        y: this.zeroPoints[1].y + box.z * this.scale,
        width: box.length * this.scale,
        length: box.height * this.scale,
        angle: 0,
        rotationPoint: box.rotationPoint,
      },
    ];
  }

  calculateLine(line: ILine) {
    let zeroPoint;

    switch (line.plan) {
      case 'top':
        zeroPoint = this.zeroPoints[0];
        break;
      case 'left':
        zeroPoint = this.zeroPoints[1];
        break;
    }
    return [
      {
        name: line.name,
        type: 'line',
        plan: line.plan,
        x: zeroPoint.x + line.x * this.scale,
        y: zeroPoint.y + line.y * this.scale,
        x2: zeroPoint.x + line.x2 * this.scale,
        y2: zeroPoint.y + line.y2 * this.scale,

      }
      ];
  }

}
