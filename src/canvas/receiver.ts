import {IBlock, ISphere} from './model';

export class Receiver {
  private blocks: IBlock[];
  private block: IBlock;
  private blockIndex: number;


  constructor(blocks: IBlock[]) {
    this.blocks = blocks;
    this.blockIndex = this.blocks.findIndex((block) => block.name === 'receiver');
    this.block = this.blocks[this.blockIndex];
  }

  public getObjects(): IBlock[] {
    this.blocks[this.blockIndex] = <ISphere> {
      name: 'receiver',
      type: 'sphere',
      x: this.block.x,
      y: this.block.y,
      z: this.block.z,
      radius: 10,
      draggable: true,
    };

    return this.blocks;

  }




}
