import {ICoordinates} from './receiver.model';
import {IBlock, IBox} from './model';

export class Speaker {
  private coordinates;
  private name;
  private readonly blocks: IBlock[];
  private blockIndex: number;
  private block: IBox;

  constructor(blocks: IBlock[]) {
    this.blocks = blocks;
  }

  public setName(name: string) {
    this.name = name;
    this.blockIndex = this.blocks.findIndex((block) => block.name === this.name);
    this.block = <IBox> this.blocks[this.blockIndex];
  }

  public getObjects(): IBlock[] {
    this.blocks[this.blockIndex] = <IBox>
      {
        name: this.name,
        type: 'box',
        x: this.block.x,
        y: this.block.y,
        z: this.block.z,
        width: 22,
        length: 25,
        height: 32,
        angle: this.getSpeakerAngle(),
        rotationPoint: 'front-center',
        draggable: true,
      };

    return this.blocks;
  }

  private getSpeakerAngle() {
    const receiverIndex = this.blocks.findIndex((block) => block.name === 'receiver');
    const receiverCoordinates = this.blocks[receiverIndex];
    const x = this.block.x + (this.block.width / 2) - receiverCoordinates.x;
    const y = this.block.y + (this.block.length) - receiverCoordinates.y;

    const angle = Math.atan(Math.abs(x / y));

    if (x > 0 && y < 0) {
      return angle;
    } else if (x < 0 && y < 0) {
      return -angle;
    } else if (x < 0 && y > 0) {
      return Math.PI + angle;
    } else if (x > 0 && y > 0) {
      return Math.PI - angle;
    }
  }




}
