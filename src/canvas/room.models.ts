export interface IRoomDimensions {
  width: number;
  length: number;
  height: number;
}
