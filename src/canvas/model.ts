export interface IPoint {
  x: number;
  y: number;
}

export interface IBlock {
  name: string;
  type: string;
  draggable: boolean;
  x: number;
  y: number;
  z: number;
}

export interface ILine extends IBlock {
  type: 'line';
  x2: number;
  y2: number;
  z2: number;
  plan: 'top' | 'left';
}

export interface IBox extends IBlock {
  type: 'box';
  width: number;
  length: number;
  height: number;
  angle?: number;
  rotationPoint: 'front-center';
}

export interface ISphere extends IBlock {
  type: 'sphere';
  radius: number;
}

export interface IShape {
  type: string;
  name: string;
  plan: 'top' | 'left';
  x: number;
  y: number;
  draggable: boolean;
}

export interface IRectangle extends IShape {
  type: 'rectangle';
  width: number;
  length: number;
  angle?: number;
  rotationPoint?: string;
}

export interface ICircle extends IShape {
  type: 'circle';
  radius: number;
}

export interface IBoundaries {
  maxX: number;
  minX: number;
  maxY: number;
  minY: number;
}
