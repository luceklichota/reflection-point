export interface IOffset {
  x: number;
  y: number;
}

export interface ICoordinates {
  x: number;
  y: number;
  z: number;
}

