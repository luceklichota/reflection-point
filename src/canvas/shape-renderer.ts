import {ICircle, ILine, IPoint, IRectangle, IShape} from './model';
import {Rectangle} from './shapes/rectangle';

export class ShapeRenderer {
  constructor(private ctx: CanvasRenderingContext2D) {

  }

  render(shapes: IShape[]) {
    shapes.forEach((shape: IShape) => {
      switch (shape.type) {
        case 'circle':
          this.renderCircle(<ICircle> shape);
          break;
        case 'rectangle':
          this.renderRectangle(<IRectangle> shape);
          break;
        case 'line':
          this.renderLine(<ILine> shape);
          break;
      }
    });
  }

  renderCircle(circle: ICircle) {
    this.ctx.beginPath();
    this.ctx.arc(circle.x, circle.y, circle.radius, 0, 2 * Math.PI);
    this.ctx.stroke();
  }

  renderLine(line: ILine) {
    this.ctx.beginPath();
    this.ctx.moveTo(line.x, line.y);
    this.ctx.lineTo(line.x2, line.y2);
    this.ctx.stroke();
  }

  renderRectangle(rectangle: IRectangle) {
    const rotationPoint = Rectangle.getRotationPoint(rectangle);
    if (rotationPoint) {
      this.ctx.translate(rotationPoint.x, rotationPoint.y);
      this.ctx.rotate(rectangle.angle);
      this.ctx.translate(-rotationPoint.x, -rotationPoint.y);
      this.ctx.beginPath();
    }


    this.ctx.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.length);
    this.ctx.stroke();
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
  }
}
