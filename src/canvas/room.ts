import {IRoomDimensions} from './room.models';
import {IPoint, IRectangle} from './model';

export class Room {
  private roomDimensions: IRoomDimensions;
  private scale = 1;
  private offset = 0;

  public setDimensions(dimensions: IRoomDimensions) {
    this.roomDimensions = dimensions;
  }

  public setScale(scale: number) {
    this.scale = scale;
  }

  public setOffset(offset: number) {
    this.offset = offset;
  }

  public getObjects(): IRectangle[] {
    return [this.rectangleTop(), this.rectangleLeft()];
  }

  public rectangleTop(): IRectangle {
    const dimensions = this.getRoomDimensionsScaled();
    return {
      type: 'rectangle',
      name: 'room',
      plan: 'top',
      draggable: false,
      x: this.offset,
      y: this.offset,
      width: dimensions.width,
      length: dimensions.length,
    };
  }

  private rectangleLeft(): IRectangle {
    const dimensions = this.getRoomDimensionsScaled();
    return {
      type: 'rectangle',
      name: 'room',
      plan: 'left',
      draggable: false,
      x: this.offset * 2 + dimensions.width,
      y: this.offset,
      width: dimensions.length,
      length: dimensions.height,
    };
  }

  public getZeroPoints(): IPoint[] {
    const dimensions = this.getRoomDimensionsScaled();
    return [
      {
        x: this.offset,
        y: this.offset
      },
      {
        x: this.offset * 2 + dimensions.width,
        y: this.offset
      },
    ];
  }

  public getRoomDimensionsScaled(): IRoomDimensions {
    return {
      width: this.roomDimensions.width * this.scale,
      length: this.roomDimensions.length * this.scale,
      height: this.roomDimensions.height * this.scale
    };
  }
}
