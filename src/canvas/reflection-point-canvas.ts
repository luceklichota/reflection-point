import {IRoomDimensions} from './room.models';
import {Room} from './room';
import {Receiver} from './receiver';
import {IBlock, IBoundaries, IBox, ICircle, IPoint, IRectangle, IShape, ISphere} from './model';
import {BlockToShapeConverter} from './block-to-shape-converter';
import {ShapeRenderer} from './shape-renderer';
import {fromEvent} from 'rxjs';
import {DragChecker} from './drag-checker';
import {filter} from 'rxjs/operators';
import {Circle} from './shapes/circle';
import {Speaker} from './speaker';
import {Rectangle} from './shapes/rectangle';
import {Reflection} from './reflection';

export class ReflectionPointCanvas {
  private roomOffset = 20;
  private scale = 1;
  private roomDimensions: IRoomDimensions;
  private blocks: IBlock[];
  private shapes: IShape[];
  private zeroPoints: IPoint[];
  private renderer: ShapeRenderer;
  private isDrag: boolean;
  private dragShape: IShape;
  private blockDimensions: any;


  constructor(private ctx: CanvasRenderingContext2D) {
    this.renderer = new ShapeRenderer(ctx);

    fromEvent(document, 'mousedown').subscribe((event: MouseEvent) => {
      const point = this.getClickCoordinates(event);
      const shape = DragChecker.isClicked(this.shapes, point);

      if (shape) {
        this.isDrag = true;
        this.dragShape = shape;
      }
    });

    fromEvent(document, 'mouseup').subscribe(event => {
      this.isDrag = false;
      this.dragShape = null;
    });

    fromEvent(document, 'mousemove').pipe(
      filter(() => this.isDrag)
    ).subscribe((event: MouseEvent) => {
      this.dragShape = this.shapes.find((shape) => shape.name === this.dragShape.name && shape.plan === this.dragShape.plan);
      this.dimensionsOnDrag(this.getPointRelativeToCanvas(event));
    });
  }

  getPointRelativeToCanvas(e: MouseEvent) {
    return {
      x: e.clientX - this.ctx.canvas.offsetLeft,
      y: e.clientY - this.ctx.canvas.offsetTop
    };
  }

  calculateRoomCoordinates() {
    const room = new Room();
    room.setScale(this.scale);
    room.setDimensions(this.roomDimensions);
    room.setOffset(this.roomOffset);
    this.zeroPoints = room.getZeroPoints();

    return room.getObjects();
  }

  getBlocks() {
    const receiver = new Receiver(this.blocks);
    this.blocks = receiver.getObjects();

    const speakerL = new Speaker(this.blocks);
    speakerL.setName('speakerL');
    this.blocks = speakerL.getObjects();

    const speakerR = new Speaker(this.blocks);
    speakerR.setName('speakerR');
    this.blocks = speakerR.getObjects();

    const reflectionL = new Reflection(this.blocks, this.roomDimensions);
    reflectionL.setName('reflection');
    this.blocks = reflectionL.getObjects();

  }

  setRoomDimensions(dimensions: IRoomDimensions) {
    this.roomDimensions = dimensions;
    this.setScale();
  }

  render() {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.renderer.render(this.calculateRoomCoordinates());
    const blockToShapeConverter = new BlockToShapeConverter(this.scale, this.zeroPoints);
    this.shapes = blockToShapeConverter.convert(this.blocks);

    this.renderer.render(this.shapes);
  }

  setScale() {
    const dimensions = this.roomDimensions;

    const canvasWidth = this.ctx.canvas.width;
    const canvasHeight = this.ctx.canvas.height;
    const canvasRatio = canvasWidth / canvasHeight;

    const drawingWidth = dimensions.width + dimensions.length;
    const drawingLength = Math.max(dimensions.length, dimensions.height);
    const drawingRatio = drawingWidth / drawingLength;

    let scale: number;
    if (canvasRatio > drawingRatio) {
      scale = (canvasHeight - 2 * this.roomOffset) / drawingLength;
    } else {
      scale = (canvasWidth - 3 * this.roomOffset) / drawingWidth;
    }

    this.scale = scale;
  }

  getClickCoordinates(e: MouseEvent): IPoint {
    return {
      x: e.clientX - this.ctx.canvas.offsetLeft,
      y: e.clientY - this.ctx.canvas.offsetTop
    };
  }

  dimensionsOnDrag(mousePoint) {
    let relativeMousePoint: IPoint;
    let planIndex: number;
    let boundaries: IBoundaries;
    const index = this.blocks.findIndex((block) => block.name === this.dragShape.name);

    switch (this.dragShape.plan) {
      case 'top':
        relativeMousePoint = {
          x: mousePoint.x - this.zeroPoints[0].x,
          y: mousePoint.y - this.zeroPoints[0].y,
        };
        planIndex = 0;
        break;
      case 'left':
        relativeMousePoint = {
          x: mousePoint.x - this.zeroPoints[1].x,
          y: mousePoint.y - this.zeroPoints[1].y,
        };
        planIndex = 1;
        break;
    }

    switch (this.dragShape.type) {
      case 'circle':
        boundaries = Circle.getBoundaries(<ICircle> this.dragShape, this.calculateRoomCoordinates()[planIndex]);
        break;
      case 'rectangle':
        boundaries = Rectangle.getBoundaries(<IRectangle> this.dragShape, this.calculateRoomCoordinates()[planIndex]);
        const rectangle = <IRectangle> this.dragShape;
        relativeMousePoint.x = relativeMousePoint.x - rectangle.width / 2;
        relativeMousePoint.y = relativeMousePoint.y - rectangle.length;
        break;

    }

    switch (planIndex) {
      case 0:
        this.blocks[index].x = Math.min(Math.max(relativeMousePoint.x, boundaries.minX), boundaries.maxX) / this.scale;
        this.blocks[index].y = Math.min(Math.max(relativeMousePoint.y, boundaries.minY), boundaries.maxY) / this.scale;
        break;
      case 1:
        this.blocks[index].y = Math.min(Math.max(relativeMousePoint.x, boundaries.minX), boundaries.maxX) / this.scale;
        this.blocks[index].z = Math.min(Math.max(relativeMousePoint.y, boundaries.minY), boundaries.maxY) / this.scale;
        break;
    }

    this.getBlocks();

    this.render();
  }

  setBlocksDimensions() {
    this.blocks =
      [
        <ISphere> {
          draggable: true,
          name: 'receiver',
          radius: 10,
          type: 'sphere',
          x: this.roomDimensions.width / 2,
          y: this.roomDimensions.length / 2,
          z: 100,
        },
        <IBox> {
          draggable: true,
          height: 32,
          length: 25,
          name: 'speakerL',
          rotationPoint: 'front-center',
          type: 'box',
          width: 22,
          x: this.roomDimensions.width / 3 - 11,
          y: 40,
          z: 100,
        },
        <IBox> {
          draggable: true,
          height: 32,
          length: 25,
          name: 'speakerR',
          rotationPoint: 'front-center',
          type: 'box',
          width: 22,
          x: (this.roomDimensions.width / 3) * 2 - 11,
          y: 40,
          z: 100,
        },
      ];
  }
}
