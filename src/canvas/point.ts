import {IPoint} from './model';

export class Point {
  static pointAfterRotation(point: IPoint, rotationPoint: IPoint, angle: number): IPoint {
    return {
      x: (point.x - rotationPoint.x) * Math.cos(angle) - (point.y - rotationPoint.y) * Math.sin(angle) + rotationPoint.x,
      y: (point.x - rotationPoint.x) * Math.sin(angle) + (point.y - rotationPoint.y) * Math.cos(angle) + rotationPoint.y
    };
  }
}
