import {IBoundaries, IPoint, IRectangle} from '../model';
import {Point} from '../point';

export class Rectangle {
  static isClicked(rectangle: IRectangle, point: IPoint) {

    const x = point.x;
    const y = point.y;

    let inside = false;

    const rectanglePoints = Rectangle.getRectanglesPoints(rectangle);

    for (let i = 0; i < rectanglePoints.length; i++) {
      const nextCorner = i === 3 ? 0 : i + 1;
      const xi = rectanglePoints[i].x;
      const yi = rectanglePoints[i].y;
      const xj = rectanglePoints[nextCorner].x;
      const yj = rectanglePoints[nextCorner].y;

      const intersect = ((yi > y) !== (yj > y))
        && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);

      if (intersect) {
        inside = !inside;
      }
    }

    return inside;
  }

  static getRectanglesPoints(rectangle: IRectangle): IPoint[] {
    const points = [];

    const angle = rectangle.angle;
    const rectanglePoints = [
      Point.pointAfterRotation({
        x: rectangle.x,
        y: rectangle.y
      }, Rectangle.getRotationPoint(rectangle), angle),
      Point.pointAfterRotation({
        x: rectangle.x + rectangle.width,
        y: rectangle.y
      }, Rectangle.getRotationPoint(rectangle), angle),
      Point.pointAfterRotation({
        x: rectangle.x + rectangle.width,
        y: rectangle.y + rectangle.length
      }, Rectangle.getRotationPoint(rectangle), angle),
      Point.pointAfterRotation({
        x: rectangle.x,
        y: rectangle.y + rectangle.length
      }, Rectangle.getRotationPoint(rectangle), angle),
    ] as IPoint[];

    return rectanglePoints;
  }

  static getRotationPoint(rectangle: IRectangle) {
    let point: IPoint;

    switch (rectangle.rotationPoint) {
      case 'front-center':
        point = {
          x: rectangle.x + (rectangle.width / 2),
          y: rectangle.y + rectangle.length
        };
    }


    return point;
  }

  static getBoundaries(rectangle: IRectangle, room: IRectangle): IBoundaries {
      const points = this.getRectanglesPoints(rectangle);
      const rotationPoint = this.getRotationPoint(rectangle);
      const relativeCoords = {
        minX: 0,
        minY: 0,
        maxX: 0,
        maxY: 0,
      };

      points.forEach(point => {
        if (point.x - rotationPoint.x < relativeCoords.minX) {
          relativeCoords.minX = point.x - rotationPoint.x;
        }
        if (point.x - rotationPoint.x > relativeCoords.maxX) {
          relativeCoords.maxX = point.x - rotationPoint.x;
        }
        if (point.y - rotationPoint.y > relativeCoords.maxY) {
          relativeCoords.maxY = point.y - rotationPoint.y;
        }
        if (point.y - rotationPoint.y < relativeCoords.minY) {
          relativeCoords.minY = point.y - rotationPoint.y;
        }
      });

      const extremeCoords = {
        minX: Math.abs(relativeCoords.minX) - rectangle.width / 2,
        minY: Math.abs(relativeCoords.minY) - rectangle.length,
        maxX: room.width - Math.abs(relativeCoords.maxX) - rectangle.width / 2,
        maxY: room.length - Math.abs(relativeCoords.maxY) - rectangle.length,
      };

      return extremeCoords;
    }
}
