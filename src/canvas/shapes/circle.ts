import {IBoundaries, ICircle, IPoint, IRectangle} from '../model';

export class Circle {
  static isClicked(circle: ICircle, point: IPoint) {
    return Math.sqrt((point.x - circle.x) ** 2 + (point.y - circle.y) ** 2) < circle.radius;
  }

  static getBoundaries(circle: ICircle, room: IRectangle): IBoundaries {
    return {
      maxX: room.width - circle.radius,
      minX: circle.radius,
      maxY: room.length - circle.radius,
      minY: circle.radius,
    };
  }
}
