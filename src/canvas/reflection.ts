import {IBlock, IBox, ILine} from './model';
import {IRoomDimensions} from './room.models';

export class Reflection {
  private blocks: IBlock[];
  private name: string;
  private blockIndex: number;
  private roomDimensions: any;

  private speakers = [
    'speakerL',
    'speakerR',
  ];

  constructor(blocks: IBlock[], roomDimensions: IRoomDimensions) {
    this.roomDimensions = roomDimensions;
    this.blocks = blocks;
  }

  public setName(name: string) {
    this.name = name;
    this.blocks = this.blocks.filter(block => !block.name.includes(name));
  }

  private getSpeaker(name: string) {
    const speakerIndex = this.blocks.findIndex((block) => block.name === name);
    return this.blocks[speakerIndex];
  }

  private getReceiver() {
    const receiverIndex = this.blocks.findIndex((block) => block.name === 'receiver');
    return this.blocks[receiverIndex];
  }

  public getObjects(): IBlock[] {

    this.speakers.forEach((name, index) => {
      const speaker = <IBox> this.getSpeaker(name);
      const receiver = this.getReceiver();

      let dx2 = this.roomDimensions.width - (speaker.x + speaker.width/2);
      let dx3 = this.roomDimensions.width - receiver.x;
      const dy = receiver.y - (speaker.y + speaker.length) ;

      const reflectionPoint1 = {
        x: this.roomDimensions.width,
        y: speaker.y + speaker.length + (dx2 * dy) / (dx3 + dx2),
        z: receiver.z,
      };

      dx2 = speaker.x;
      dx3 = receiver.x;

      const reflectionPoint2 = {
        x: 0,
        y: speaker.y + speaker.length + (dx2 * dy) / (dx3 + dx2),
        z: receiver.z,
      };

      this.blocks.push(<ILine> {
        type: 'line',
        name: this.name + index * 4,
        draggable: false,
        x: reflectionPoint1.x,
        y: reflectionPoint1.y,
        z: reflectionPoint1.z,
        x2: speaker.x + speaker.width/2,
        y2: speaker.y + speaker.length,
        z2: speaker.z,
        plan: 'top'
      });

      this.blocks.push(<ILine> {
        type: 'line',
        name: this.name + index * 4 + 1,
        draggable: false,
        x: receiver.x,
        y: receiver.y,
        z: receiver.z,
        x2: reflectionPoint1.x,
        y2: reflectionPoint1.y,
        z2: reflectionPoint1.z,

        plan: 'top'
      });


      this.blocks.push(<ILine> {
        type: 'line',
        name: this.name + index * 4 + 2,
        draggable: false,
        x: reflectionPoint2.x,
        y: reflectionPoint2.y,
        z: reflectionPoint2.z,
        x2: speaker.x + speaker.width / 2,
        y2: speaker.y + speaker.length,
        z2: speaker.z,
        plan: 'top'
      });

      this.blocks.push(<ILine> {
        type: 'line',
        name: this.name + index * 4 + 3,
        draggable: false,
        x: receiver.x,
        y: receiver.y,
        z: receiver.z,
        x2: reflectionPoint2.x,
        y2: reflectionPoint2.y,
        z2: reflectionPoint2.z,

        plan: 'top'
      });
    });


    return this.blocks;
  }
}
