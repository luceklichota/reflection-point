import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ReflectionPointCanvas} from '../canvas/reflection-point-canvas';
import {DimensionsService} from './dimensions.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  @ViewChild('canvas', {static: true}) canvas: ElementRef<HTMLCanvasElement>;
  ctx: CanvasRenderingContext2D;
  public dimensionsForm: FormGroup;
  private drawing: ReflectionPointCanvas;

  constructor(private formBuilder: FormBuilder, private dimensionsService: DimensionsService) {

  }

  onChanges() {
    this.dimensionsForm.valueChanges.subscribe(val => {
      this.drawing.setRoomDimensions({width: parseInt(val.width, null), length: parseInt(val.length), height: parseInt(val.height)});
      this.drawing.setBlocksDimensions();
      this.drawing.getBlocks();
      this.drawing.render();
    });
  }

  ngOnInit() {
    this.ctx = this.canvas.nativeElement.getContext('2d');

    this.dimensionsForm = this.formBuilder.group({
      width: [340],
      length: [400],
      height: [230]
    });

    this.drawing = new ReflectionPointCanvas(this.ctx);
    this.onChanges();
    this.dimensionsForm.updateValueAndValidity({onlySelf: false, emitEvent: true});

  }
}
